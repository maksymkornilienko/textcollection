package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
        Map<String, Integer> words = new HashMap<String, Integer>();
        String[] subStr;
        String str="";
        for (String line : Files.readAllLines(Paths.get("loremIpsum.txt"))) {
            str = line.replaceAll("[^\\da-zA-Zа-яёА-ЯЁ ]", "");
        }
        subStr = str.split(" ");
        for (String word: subStr){
            Integer count=words.get(word);
            if (count == null){
                count=0;
            }
            words.put(word,count+1);
        }
        words.forEach((k ,v )->{
            System.out.println("Key: " + k + ", Value: " + v);
        });
    }
}
